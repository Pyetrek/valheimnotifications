provider "aws" {
  region = "ca-central-1"
}

variable ecs_cluster_name {}
variable ecs_service_name {}
variable discord_webhook_url {
    sensitive = true
}

resource "aws_iam_role" "iam_for_lambda" {
  name = "ValheimServerNotifications"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "iam_for_lambda_policy" {
  name = "ValheimServerNotificationPolicy"
  role = aws_iam_role.iam_for_lambda.id

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "ecs:ListTasks",
          "ecs:DescribeTasks",
          "ec2:DescribeNetworkInterfaces"
        ]
        Effect   = "Allow"
        Resource = "*"
      },
    ]
  })
}

data "archive_file" "lambda_zip" {
    type        = "zip"
    source_dir  = "src"
    output_path = "lambda.zip"
}

resource "aws_lambda_function" "notification_lambda" {
  filename      = "lambda.zip"
  function_name = "ValheimServerNotifications"
  role          = aws_iam_role.iam_for_lambda.arn
  handler       = "handler.lambda_handler"

  source_code_hash = data.archive_file.lambda_zip.output_base64sha256

  runtime = "python3.8"

  environment {
    variables = {
      ECS_CLUSTER_NAME = var.ecs_cluster_name
      ECS_SERVICE_NAME = var.ecs_service_name
      DISCORD_WEBHOOK_URL = var.discord_webhook_url
    }
  }
}

resource "aws_cloudwatch_event_rule" "task_state_change" {
  name        = "ValheimServerTaskStateChange"
  description = "captures ecs task state change events"

  event_pattern = <<EOF
{
  "source": [
    "aws.ecs"
  ],
  "detail-type": [
    "ECS Task State Change"
  ]
}
EOF
}

resource "aws_cloudwatch_event_target" "task_state_change" {
  arn  = aws_lambda_function.notification_lambda.arn
  rule = aws_cloudwatch_event_rule.task_state_change.id
}

resource "aws_lambda_permission" "allow_cloudwatch_notification_lambda" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.notification_lambda.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.task_state_change.arn
}
