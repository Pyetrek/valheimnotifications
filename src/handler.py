import json, requests, os, boto3

def pagingDiscordServer(text_message):
    webhook_url = os.environ['DISCORD_WEBHOOK_URL']
    return requests.post(webhook_url, data=json.dumps({ "content": text_message }), headers={ 'Content-Type': 'application/json',})


def lambda_handler(event, context):
    if event['detail-type'] == 'ECS Task State Change' and event['detail']['lastStatus'] == 'RUNNING':
        ecs_cluster_name = os.environ['ECS_CLUSTER_NAME']
        ecs_service_name = os.environ['ECS_SERVICE_NAME']
        ecs_client = boto3.client('ecs')
        ecs_task_arns = ecs_client.list_tasks(
            cluster=ecs_cluster_name,
            serviceName=ecs_service_name
        )['taskArns']

        ecs_task = ecs_client.describe_tasks(
            cluster=ecs_cluster_name,
            tasks=ecs_task_arns
        )['tasks'][0]
        eni_id = [d for d in ecs_task['attachments'][0]['details'] if d['name'] == 'networkInterfaceId'][0]['value']

        ec2_client = boto3.client('ec2')
        eni = ec2_client.describe_network_interfaces(NetworkInterfaceIds=[eni_id])['NetworkInterfaces'][0]
        
        ip = eni['Association']['PublicIp']

        pagingDiscordServer(f"Server Connection Info: {ip}:2456")
